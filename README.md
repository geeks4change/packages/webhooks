# G4C WebHooks

## For all webhooks

### Webhook server setup

* Setup DNS for WEBHOOKS.TLD
* Set server vhost for WEBHOOKS.TLD
* Clone this repo and point docroot to its web directory
  (so that top level directory is not web visible)
* Make *webhook setup* above
* Meke *personal setup* above
* Have fun

### Personal Setup

* Log in to gitlab
* Go to https://gitlab.com/profile/personal_access_tokens
* Create a token with full api access
* On the webhook server, save it in a file named private/gitlab-user-tokens/YOUR-NAME

## WebHook: Gitlab Comment Todo

Add magic todo-subscribers to issue title like this:

* *Blah title =>all*
* *Blah title =>assignees*
* *Blah title =>geek-anna+geek-bert*

Then you get a TODO for every comment (not your own).
(Now you may want to kill email notifications under https://gitlab.com/profile/notifications)

### WebHook setup (per project)

* Create a secret webhook token (for all projects) and store it under private/gitlab-webhook-tokens/gitlab-comment-to-todo
* Go to https://gitlab.com/[PROJECT]/-/settings/integrations and add a project
  webhook for *comments* to https://WEBHOOKS.TLD/gitlab-comment-to-todo.php
  and add your secret webhook token.
* For testing purposes you can add `?also-self` to get TODOs for own comments.

You have to do this for every single project, unless you have a plan with group webhooks.

## WebHook: Gitlab Commit To Issue Comment

Makes a comment on a (configurable) issue for every push (one or more commits).
(You can then subscribe to that issue.)

### WebHook setup (per project)

* Create a secret webhook token (for all projects) and store it under private/gitlab-webhook-tokens/gitlab-commit-to-issue-comment
* Go to https://gitlab.com/[PROJECT]/-/settings/integrations and add a project
  webhook for *push* events to https://WEBHOOKS.TLD/gitlab-commit-to-issue-comment.php?issue=1337
  (for issue no. 1337) and add your secret webhook token.

You have to do this for every single project, unless you have a plan with group webhooks.
