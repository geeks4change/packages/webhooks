<?php

use GuzzleHttp\Client;

require __DIR__ . '/../vendor/autoload.php';

function snakelize($input) {
// 'MergeRequest' => 'merge_request'
// https://stackoverflow.com/a/35719689/606859
  return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $input));
}

function userTokens() {
  $tokens = [];
  foreach (glob(__DIR__ . '/../private/gitlab-user-tokens/*') as $path) {
    $basename = basename($path);
    if (substr($path, 0, 1) === '.') {
      continue;
    }
    $tokens[$basename] = trim(file_get_contents($path));
  }
  return $tokens;
}

// Only with correct token.
$gitlabToken = $_SERVER['HTTP_X_GITLAB_TOKEN'];
if ($gitlabToken !== trim(file_get_contents(__DIR__ . '/../private/gitlab-webhook-tokens/gitlab-commit-to-issue-comment'))) {
  http_response_code(401);
  return;
}

// Only for commit.
$data = json_decode(file_get_contents('php://input'), TRUE);
// https://gitlab.com/help/user/project/integrations/webhooks#comment-on-issue
// https://gitlab.com/help/user/project/integrations/webhooks#comment-on-merge-request
if (($data['object_kind'] ?? '') !== 'push') {
  http_response_code(400);
  return;
}

// Get all data.
// https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#push-events
$projectId = $data['project_id'];
$issueIid = $_GET['issue'] ?? NULL;
if (!$issueIid) {
  http_response_code(400);
  return;
}
$pushUserName = $data['user_username'];

$userTokens = userTokens();
$userName = isset($userTokens[$pushUserName]) ? $pushUserName : key($userTokens);
$token = $userTokens[$userName];
$commitsCount = $data['total_commits_count'];

$bodyParts = [];
$bodyParts[] = "Added $commitsCount commits:\n\n";

foreach ($data['commits'] as $commit) {
  $url = $commit['url'];
  $title = $commit['title'];
  $authorName = $commit['author']['name'];
  $bodyParts[] = "* <a href=\"$url\">$title</a> by $authorName";
}

// https://docs.gitlab.com/ee/api/notes.html#create-new-issue-note
$todoData = [
  'body' => implode("\n", $bodyParts),
];
$clientOptions = ['headers' => ['Private-Token' => $token], 'debug' => fopen(__DIR__ . '/../guzzledebug.txt', 'w')];
$client = new Client(['base_uri' => 'https://gitlab.com/api/v4/']);
$client->post("projects/$projectId/issues/$issueIid/notes", $clientOptions + ['json' => $todoData]);
http_response_code(201);
