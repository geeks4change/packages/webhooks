<?php

use GuzzleHttp\Client;

require __DIR__ . '/../vendor/autoload.php';

function snakelize($input) {
  // 'MergeRequest' => 'merge_request'
  // https://stackoverflow.com/a/35719689/606859
  return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $input));
}

function userTokens() {
  $tokens = [];
  foreach (glob(__DIR__ . '/../private/gitlab-user-tokens/*') as $path) {
    $basename = basename($path);
    if (substr($path, 0, 1) === '.') {
      continue;
    }
    $tokens[$basename] = trim(file_get_contents($path));
  }
  return $tokens;
}

// Only with correct token.
$gitlabToken = $_SERVER['HTTP_X_GITLAB_TOKEN'];
if ($gitlabToken !== trim(file_get_contents(__DIR__ . '/../private/gitlab-webhook-tokens/gitlab-comment-to-todo'))) {
  http_response_code(401);
  return;
}

// Only for comments.
$data = json_decode(file_get_contents('php://input'), TRUE);
// https://gitlab.com/help/user/project/integrations/webhooks#comment-on-issue
// https://gitlab.com/help/user/project/integrations/webhooks#comment-on-merge-request
if (($data['object_kind'] ?? '') !== 'note') {
  http_response_code(400);
  return;
}

// Only on issues / MRs.
$noteableType = $data['object_attributes']['noteable_type'] ?? '';
$noteableType = snakelize($noteableType);

// Get all data.
$noteable = $data[$noteableType] ?? NULL;
$noteableTitle = $noteable['title'];
// Yes, iid. "Internal", project-specific id.
$noteableIid = $noteable['iid'];
$projectId = $data['project_id'];
$noteBody = $data['object_attributes']['note'] ?? NULL;
$noteUserName = $data['user']['username'] ?? NULL;
$assigneeIds = $noteable['assignee_ids'] ?? NULL;

// Find our magic subscription: =user1+user2=
$isDiscussion = preg_match('/=>([a-zA-Z0-9._\-+]*)/ui', $noteableTitle, $matches)
  && ($magicSubscribers = explode('+', $matches[1]));
if (!$isDiscussion) {
  http_response_code(200);
  return;
}

$client = new Client(['base_uri' => 'https://gitlab.com/api/v4/']);
foreach (userTokens() as $userName => $token) {
  $doIt = FALSE;
  if ($userName === $noteUserName && !isset($_GET['also-self'])) {
    $doIt = FALSE;
  }
  elseif (in_array('all', $magicSubscribers)) {
    $doIt = TRUE;
  }
  elseif (in_array($userName, $magicSubscribers)) {
    $doIt = TRUE;
  }
  elseif (in_array('assignees', $magicSubscribers)) {
    $clientOptions = ['headers' => ['Private-Token' => $token], 'debug' => fopen(__DIR__ . '/../guzzledebug1.txt', 'w')];
    // Get user id of current user.
    // https://docs.gitlab.com/ee/api/users.html#list-current-user-for-normal-users
    $userResponse = $client->get('user', $clientOptions);
    $userJson = $userResponse->getBody()->getContents();
    $userData = json_decode($userJson, TRUE);
    $userId = $userData['id'];
    $doIt = in_array($userId, $assigneeIds);
  }

  if (!$doIt) {
    continue;
  }
  // If subscribed, create a todo.
  // https://docs.gitlab.com/ee/api/issues.html#create-a-todo
  // https://docs.gitlab.com/ee/api/merge_requests.html#create-a-todo
  // Currently body seems to be ignored.
  // Upstream: https://gitlab.com/gitlab-org/gitlab/issues/205916
  $todoData = [
    'body' => $noteBody,
  ];
  $clientOptions = ['headers' => ['Private-Token' => $token], 'debug' => fopen(__DIR__ . '/../guzzledebug.txt', 'w')];
  $response = $client->post("projects/$projectId/{$noteableType}s/$noteableIid/todo", $clientOptions + ['json' => $todoData]);
  http_response_code(201);
}
